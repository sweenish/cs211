#include <iostream>

/*
 * Changes argument and prints it
 */
void change_value(int x);

/*
 * Changes argument and prints it
 */
void change_reference(int& x);

int main()
{
    int y = 42;

    std::cout << "y is " << y << '\n';
    change_value(y);
    std::cout << "y is " << y << '\n';

    std::cout << "\n\n";

    std::cout << "y is " << y << '\n';
    change_reference(y);
    std::cout << "y is " << y << '\n';

    return 0;
}

void change_value(int x)
{
    x = 24;
    std::cout << "In change_value, x is " << x << '\n';

    return;
}

void change_reference(int& x)
{
    x = 24;
    std::cout << "in change_reference, x is " << x << '\n';

    return;
}
