#include <iostream>

double min_of_two(double a, double b)
{
    if (a < b) {
        return a;
    } else {
        return b;
    }
    // return (a < b) ? a : b;
}

int main()
{
    double firstEntry;
    double secondEntry;
    std::cout << "Enter a number: ";
    std::cin >> firstEntry;
    std::cout << "Enter another number: ";
    std::cin >> secondEntry;

    double result = min_of_two(firstEntry, secondEntry);

    std::cout << "The smaller number was: " << result << '\n';

    return 0;
}
