#include <cctype>  // tolower()
#include <cmath>
#include <iomanip>  // std::put_money()
#include <iostream>
#include <locale>

/*
 * Calculates the area of a circle
 *
 * Parameters:
 *   diameter: pizza diameter in inches
 *
 * Return:
 *   Area of the circle
 */
double calculate_area_of_circle(int diameter)
{
    double       radius = diameter / 2.0;
    const double pi     = 3.1415926;
    return pi * std::pow(radius, 2);
}

/*
 * Calculates the area of a rectangle. This function does not perform an
 * explicit cast to double on one of the operands becuase, unlike
 * division, information cannot be lost in the integer multiplication.
 *
 * Parameters:
 *   length: length in inches
 *   width: width in inches
 *
 * Return:
 *   Area of the rectangle
 */
double calculate_area_of_rectangle(int length, int width)
{
    return length * width;
}

/*
 * Gets input from thet user to determine shape of pizza.
 *
 * Return:
 *   'c' denotes a circular pizza
 *   'r' denotes a rectangular pizza
 */
char prompt_pizza_shape()
{
    char input;
    do {
        std::cout << "Is the pizza (c)ircular or (r)ectangular? ";
        std::cin >> input;
        input = std::tolower(input);
    } while (input != 'c' && input != 'r');

    return input;
}

/*
 * Calculate the area, given a shape. Once the function knows the shape of the
 * pizza, it prompts for the appropriate dimensions and calculates the area.
 *
 * Parameters:
 *   shape: a character that represents the shape of the pizza
 */
double use_shape_to_calculate_area(char shape)
{
    switch (shape) {
    case 'c':
        int diameter;
        std::cout << "Enter diameter: ";
        std::cin >> diameter;
        return calculate_area_of_circle(diameter);
        break;
    case 'r':
        int length;
        int width;
        std::cout << "Enter the length: ";
        std::cin >> length;
        std::cout << "Enter the width: ";
        std::cin >> width;
        return calculate_area_of_rectangle(length, width);
        break;
    default:
        return 0;
    }
}


/*
 * It's worth observing how clunky this is. Code is essentially repeated to
 * make calculations for each pizza. It's mitigated *somewhat* by the use of
 * functions. It should be plain to see that comparing more than two pizzas
 * would become a chore.
 *
 * We haven't learned the principles that can make this program much more
 * flexible and less repetitive yet.
 */
int main()
{
    char firstPizzaShape  = prompt_pizza_shape();
    double firstArea  = use_shape_to_calculate_area(firstPizzaShape);
    double firstPrice;
    std::cout << "Enter the pizza price: ";
    std::cin >> firstPrice;

    char secondPizzaShape = prompt_pizza_shape();
    double secondArea = use_shape_to_calculate_area(secondPizzaShape);
    double secondPrice;
    std::cout << "Enter the second pizza price: ";
    std::cin >> secondPrice;

    double firstPPSI  = firstPrice / firstArea;
    double secondPPSI = secondPrice / secondArea;

    // Which pizza is the better buy?
    // Compare the price per square inch, want greater value
    std::cout.imbue(std::locale("en_US.UTF-8"));
    if (firstPPSI < secondPPSI) {
        std::cout << "At $" << std::put_money(firstPPSI * 100)
                  << " per square inch, the first pizza is cheaper.\n";
    } else {
        std::cout << "At $" << std::put_money(secondPPSI * 100)
                  << " per square inch, the second pizza is cheaper.\n";
    }

    return 0;
}
