# Some Problem Solving Tips
If there is one thing that an engineer is supposed to do, that's solve problems. If it were easy,
you probably wouldn't need to be in this class. Among other engineering majors, problem solving can
more easily be taught due to the nature of the problems. In Computer Science, a given problem can
have many viable solutions, making a rote process harder to teach.

In place of a set-in-stone process, this handout presents a series of tips that will allow us to
solve problems in a repeatable and orderly manner.

## Always have a plan
* > In preparing for battle I have always found that plans are useless, but planning is 
indispensable.  -- Dwight D. Eisenhower
* The plan will likely change as you make progress on the problem, but having a plan is still key.
* Without a plan, you are hoping to get lucky.
* Planning allows you to set milestones and immediate goals and measure progress; otherwise your
goal is to solve the whole problem at once.

## Restate the Problem
* Restating the problem in your own words can lead to new and valuable insights.
* Restatement is also used to demonstrate your understanding of the problem.
* This should be the first step in any plan.

## Divide the Problem
* Dividing the problem into smaller and smaller pieces creates manageable pieces of work.
* Sometimes the division can hide itself, or be a little trickier to find.

## Start With What You Know
* A working partial solution can spark ideas about the remainder.
* Build momentum and confidence while making meaningful progress.

## Reduce the Problem
* For example, temporarily ignore a constraint that is stopping your progress.
* Solving a simpler version of the problem makes meaningful progress toward the final solution.
* Allows meaningful discussion when trying to solve a problem.
    * Better than "it doesn't work."

## Look For Analogies
* If you've solved a problem before and encounter a similar problem, a lot of the work is done.
* Analogies will rarely be 1:1, but partial analogies still solve parts of your problem.
* In this course, this particular strategy may not work often because you are building your own
store of solved problems.
    * This is why copy/pasting code or any other form of academic dishonesty is harmful.
    * Your solution storage will be nothing more than copy/paste.
    * Employers will sniff this out very quickly.
    * You need to be able to write and internalize **your own code**; this is how you build up a
    store of solutions to create analogies.
    * Reliance on the code of others now equates to relying on the code of others in the future.

## Experiment
* **Not** the same as guessing.
* Experimenting is controlled, and allows you to learn.
    * Making small changes and observing if or how output changes.
    * Scratch coding that deepens your understanding.

## Don't Get Frustrated
* Frustration is a vicious cycle.
    * You won't think as clearly.
    * You won't work as effectively.
    * Everything will take longer and seem harder.
    * This only feeds your frustration.
* *Allowing* yourself to get frustrated is giving yourself an excuse to continue to fail.
* Have a plan, and tweak/change it when necessary.
* Gather what you know and what you've tried so that you can ask a good question.
* If choosing between getting frustrated or taking a break, take the break.
    * Put the problem completely out of your head while you do something to get your blood flowing.
