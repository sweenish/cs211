# Coding Guidelines
## Comment Block
Every submission **must** include a comment block. The comment block must include your name, WSUID,
and the name of the submission (hw01, lab01, midterm, etc.). Below is an example:

```
/*
 * Firstname Lastname
 * a222a222
 * hw03
 */
```

## Style
The formatting, or style, of code is an interesting topic. It's extremely important because
well-formatted code is easier to read and parse. But it's also generally a waste of time. Time spent
focusing on ensuring that code is well-formatted is time that could be spent on more productive
tasks.

Thankfully, tools exist for pretty much every language that can format code for us. In this class,
the tool that is available is clang-format.

So instead of a long-winded breakdown of all the style choices that are subjectively preferred for
this course, there will just be a quick breakdown of how to use clang-format.

**IMPORTANT:** Formatting is a grading criteria for every code submission in this class.

## Using clang-format
This repository contains a file, `.clang-format`. This file configures clang-format. Copy it to the
root of your homework directory. Then it's simply a matter of invoking the tool on your file.
`clang-format -i WSUID_hw01.cpp` is how you'll do it. The option `-i` tells clang-format to format
the file "in place," meaning it will shuffle the contents of your actual file. By default,
clang-format would just print the formatted file to the screen.

clang-format can also be configured in both VS Code and neovim to be run automatically when you save
your file. They are not set up for this in the dev container, though. If you choose to do this for
yourself, VS Code can be configured by editing the devcontainer.json file and rebuilding. For
neovim, you'll have to edit init.vim and commit the image.

If you are using the lab and subsequently full-blown Visual Studio, it can format your code as well.
Same as above, place the configuration file in the root of your project.

## Lyrics to My Way by Frank Sinatra
If you don't want to use the clang-format tool, that is your choice. However, this means that the
formatting of your code is wholly up to you. Whatever style choices you make must be sane and
applied **consistently**.
