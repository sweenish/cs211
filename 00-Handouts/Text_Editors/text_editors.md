# Text Editors
The first thing to be said is that you can write your code in whatever program suits you. This
handout exists to provide some guidance if so desired. This list also contains editors with which I
am somewhat familiar, so I am better equipped to help troubleshoot issues with them.

## Graphical User Interface (GUI) Editors
GUI editors can feel more familiar due to having a graphical interface (think MS Word vs. working
in a terminal).

### [Visual Studio Code](https://code.visualstudio.com)
Not to be confused with Visual Studio Community, VS Code is a great extensible text editor. It is
not the most beginner friendly editor, as it does require a bit of setup out of the box, but
the payoff is a rather streamlined interface that doesn't overwhelm you with visual options.

### [Notepad++](https://notepad-plus-plus.org) (Windows)
An excellent replacement for basic text-editing needs in general, it also has a lot of power under
the hood.

### [Atom](https://atom.io) (Honorable Mention)
**Very** similar to VS Code, and also now owned by Microsoft.

## Command-Line Interface (CLI) Editors
**NOTE: I highly recommend spending some time with a powerful CLI text editor during this
semester. Familiarity with the CLI only happens with time & practice. It is a valuable skill
for any developer/engineer to have. This is especially true in today's world of containerization
and cloud computing where your only option is a CLI.**

### [vim](https://www.vim.org) or [neovim](https://neovim.io)
The two biggest names in this space are vim and emacs. I ended up on the vim side of the debate.
Vim is a configurable and *very* powerful text editor. Neovim appears to be the same as vim
on the surface, but it aims to bring many modern sensibilities to the table. With some basic
configuration changes and a cheatsheet at your side, you may find that you eventually become faster
using vim than a GUI editor since your hands will not leave the keyboard.

### [nano](https://www.nano-editor.org)
This is a much more basic text editor, but it is simpler to use because of it. Its capabilities are
sufficient for this class.

## Why I Don't Recommend IDEs
It's not that I dislike them; I am simply less familiar with them. They do simplify certain tasks,
but they are also jam-packed with options and tools that are not needed in this class. If you want
to familiarize yourself with Visual Studio Community or Xcode, that's fine. I will only note that
they don't stand out on a resume the same way that CLI experience does.

## IDEs To Avoid
There are a couple programs that I believe are detrimental.

### Geany
I'll lead off by saying that a lot of students use and are fine with Geany. I don't like it. I have
no concrete reasons, except that in my experiences helping students troubleshoot issues it always
seemed to be getting in the way.

### Dev-C++
If I were to actually disallow a program, it would be this one. The original Dev-C++ ceased
development about a decade ago and never fully supported C++11. Today, there are multiple forks
that exist, and while there is a possiblity that one of them might be good, the waters have been
muddied and better editors and/or IDEs exist anyway. So why bother.
