# Snippets from The Linux Command Line

William Shotts, The Linux Command Line: A Complete Introduction (San Francisco: No Starch Press, 2012)

## Intro to the shell
One tangential objective of this course is to become more familiar with GNU/Linux operating systems,
specifially being comfortable using the shell to issue commands to the computer.

## Keyboard shortcuts
While terminal emulators (run from a GUI environment) can incorporate the mouse, it is better to
learn keyboard shortcuts. There are many reasons for this. A main one is that many servers simply
do not provide a GUI. Neither do cloud instances or containers. In the table below, the symbol `^`
does represent `Shift + 6`, but the Control key (also abbreviated CTRL). This syntax is very common
if/when you seek out official documentation or information found online.

| Key(s) | Action |
|:---|:---|
| `[UP ARROW]` | See previous commands in history |
| `[DOWN ARROW]` | Move through history toward the present |
| `[TAB]` | Filename completion |
| `[^] + [c]` | Break, handy for force-quitting a program |
| `[^] + [a]` | Move cursor to the beginning of the line |
| `[^] + [e]` | Move cursor to the end of the line |
| `[ALT] + [f]` | Move cursor forward one word |
| `[ALT] + [b]` | Move cursor back one word |
| `[LEFT ARROW]` | Move cursor back one character |
| `[RIGHT ARROW]` | Move cursor forward one character |

## Wildcards
Wildcards allow for multiple selections of similarly named files. For example, to see all source
code files in a directory, the command `ls -al *.cpp` can be issued. The * symbol is a wildcard
that can represent **any** characters (note the plural). So, the command `ls -al *.cpp` will list
all files that end with '.cpp' in the current folder, regardless of what comes before.

The table below shows the types of wildcards that can be used:
| Wildcard | Matches |
|:---|:---|
| * | Any characters |
| ? | Any single character |
| [characters] | Any character that is a member of the set *characters* |
| [!characters] | Any character that is **not** a mamber of the set *characters* |
| [[:class:]] | Any character that is a member of the specified class |

The table below lists some common classes:
| Class | Matches |
|:---|:---|
| `[:alnum:]` | Any alphanumeric character |
| `[:alpha:]` | Any alphabetic character |
| `[:digit:]` | Any numeral |
| `[:lower:]` | Any lowercase letter |
| `[:upper:]` | Any uppercase letter |

The table below provides some examples of wildcard use:
| Pattern | Matches |
|:---|:---|
| `*` | All files in the current directory |
| `g*` | Any file that begins with a lowercase 'g' |
| `b*.txt` | Any file begins with a lowercase 'b', followed by any characters, and ending with '.txt' |
| `Data???` | Any file beginning with 'Data' followed by exactly three characters |
| `[abc]*` | Any file beginning with either 'a', 'b', or 'c' followed by any characters |
| `BACKUP.[0-9][0-9][0-9]` | Any file beginning with 'BACKUP.' followed by exactly three digits |
| `[[:upper:]]*` | Any file beginning with an uppercase letter follows by any characters |
| `[![:digit:]]*` | Any file that does not begin with a digit followed by any characters |
| `*[[:lower:]123]` | Any file ending with a lowercase letter **or** the numerals 1, 2, or 3 |

## Expansion
Expansion is the mechanism that allows wildcards to work. The short version is that anything we type
into a terminal undergoes some processing before it is actually passed on to our programs.

Specifically, wildcards us pathname expansion. All this means is the wildcards are expanded relative
to the path that is specified.

We also have `~` (tilde) expansion. The command `echo ~` will show you the absolute path to your
home directory. This is just a special case of pathname expansion.

There is also arithmetic expansion. It would look something like this: `echo $((2+2))`. We would see
'4' printed to the screen.

There is also brace expansion. Where the wildcards above allow us to select many files/folders based
on certain criteria. An example of brace expansion looks like: `mkdir hw{01..10}`. This command will
create ten folders that start with the characters 'hw' and are numbered 01, 02, ..., 09, 10 (hw01,
hw02, ..., hw09, hw10). Not every distribution treats that command exactly the same, however. 
