# A vim Editor Crash Course
vim is a powerful command line interface (CLI) text editor. It was designed to be used solely with
a keyboard, so a lot of commands and actions that we may be accustomed to are very different.
Because vim uses the keyboard for commands *and* text entry, it needs to be able to distinguish
between a command being issued, or text being typed into the document.

| Command | Action Taken |
|:---|:---|
| `vim` | Opens vim |
| `vim <filename>` | Opens \<filename\> in vim; creates file if it does not exist. |

When vim is first opened, it places the user in EDIT mode. This is the mode that allows users to
delete, cut, copy, paste, etc. Some useful commands are shown below. **IMPORTANT: All commands are
case sensitive! You will not receive any feedback that you are pressing keys until the command
executes.**

From EDIT mode, certain commands will allow you to enter INSERT mode. INSERT mode is where the
typing happens.

The table below presents some movement commands and some of the ways to enter INSERT mode.Patterns
can be observed in how commands are utilized along with movement commands.
| Key Stroke | Action Taken |
|:---|:---|
| `G` | Move cursor to the last line of the file. |
| `gg` | Move cursor to the first line of the file. |
| `5G` | Move cursor to line 5. |
| `$` | Move cursor to the end of the current line. |
| `0` (zero) | Move cursor to the beginning of the current line. |
| `x` | Delete the single character under the cursor. |
| `dd` | Cut/delete the current line. |
| `dG` | Cut/delete from the current line to the end of the file. |
| `5dd` | Including the current line, cut/delete the next 5 lines. |
| `yy` | Yank/copy the current line. NOTE: The variations shown for deletion apply to yanking. |
| `p` | Put/paste after the cursor. |
| `P` | Put/paste before the cursor. |
|`h`, `j`, `k`, `l` | Move cursor left (h), down (j), up (k), or right (l) |
| `i` | Insert cursor before current position and enter INSERT mode. NOTE: Likely to be most used. |
| `I` | Insert cursor at the beginning of the current line and enter INSERT mode. |
| `a` | Insert cursor after the current position and enter INSERT mode. |
| `A` | Insert cursor at the end of the current line and enter INSERT mode. |
| `o` | Insert a new line below the cursor, move the cursor down, and enter INSERT mode. |
| `O` | Insert a new line above the current po|

It is very likely that at some point, you'll hit some keys and end up in a thing that you don't want
to be doing. When that happens hit ESC a few times. Typically 2-3 times is enough to get you back to
EDIT mode. You will generally know mode you're in by looking in the bottom left corner of the vim
window.

There are a few other common commands worth knowing, and they are entered by typing a colon (:)
first.

| Key Strokes | Action Taken |
|:---|:---|
| `:w <filename>` | Write/save to \<filename\>. If \<filename\> is not specified, the current file is saved if it already has a name. |
| `:q` | Quit vim. You cannot quit if you have unsaved changes. |
| `:q!` | Force quit vim. This allows you to quit without saving. |
| `:wq` | Write/save and then quit vim. |
| `:e <filename>` | Opens a new buffer for \<filename\>. |

There are many many many more commands and capabilities within vim. Macro recording, search and
replace, the list goes on. But this handout should get you started.

## Outside References
[https://vim.rtorr.com](https://vim.rtorr.com)
