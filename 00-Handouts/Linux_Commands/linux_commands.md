# Short List of Linux Terminal Commands
This handout aims simply to provide some of the most common commands and options that are used to
perform basic actions on files. Any command can be further studied by reading its man pages.

## `ls`
List directory contents  
Typical Usage: `ls [OPTIONS] [FILE]`

### Popular flags
`-a` - List all files, including hidden  
`-h` - Show file sizes in a human-readable format, e.g., 3.7K instead of 3771  
`-l` - Detailed list

**Example:** `ls -alh`

### Notes
The first two items in a directory are always '.' and '..'; they mean something. '.' means 'in this
folder' or 'the current directory.' '..' means 'one folder up.'

## `cd`
Change Directory  
Example: cd [DESTINATION]

### Special locations
`..` - Go up one level in the directory tree.  
`-` - Go back to the last folder you were at.  
`~` - Go to the current user's home directory. This can also be accomplished by simply typing `cd`
and not passing any options.

### Notes
The destination can be defined relatively or explicitly. Relative paths are likely going to be more
common.

**Example:** `cd ~/cs211/hw`

## `pwd`
Prints your current directory  
Typical Usage: `pwd`

## `mv`
Move (also rename) files and/or folders  
Typical Usage: `mv [SOURCE] [DESTINATION]`

### Notes
Renaming is done by specifying the file to rename, and simply providing the new name.

**Example:** `mv hw1.cpp hw01.cpp`

## `cp`
Copy files.  
Typical Usage: `cp [FILE] [DESTINATION]`

### Notes
You can also rename the copy when specifying the destination.

## `man`
Read the manual for a command.  
Typical Usage: `man [COMMAND]`

### Notes
This allows you to see all the possible options a command can be passed. It is more of a reference
than a guide, however.

## `apropos`
Find a command.  
Typical Usage: `apropos [KEYWORD(S)]`

### Notes
Good when you feel like there should be a command to do the thing you want to do.

## `rm`
Remove files. Can also remove folders with proper options.  
Typical Usage: `rm [FLAGS] [FILENAME(S)]`

### Popular flags
`-r`, `-R` - Recursively removes files and folders. This means that if a folder is deleted, the
contents of any subfolders and the subfolders will also be deleted.  
`-f` - Forcefully remove the file.  
`-i` - Confirm every file that will be removed.  
`-v` - Verbose output.

Example: `rm somefile.cpp`

### Notes
Unlike a desktop, there is no trash bin to catch any mistakes. Always pay attention when using this
command.

## `mkdir`
Make directory.  
Typical Usage: `mkdir [DIRECTORY NAME]`

### Popular flags
`-p` - Creates parent directories if needed.

### Notes
It is a very good idea to organize your work.

## `cat`
Concatenates files together. More commonly used to print the contents of a single to the screen.  
Typical Usage: `cat [FILENAME]`

## `touch`
Update a file's timestamp. More commonly used to create an empty file with the desired name.  
Typical Usage: `touch hw10.cpp`
