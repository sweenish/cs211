/*
 * Code taken from https://www.cprogramming.com/gdb.html
 * Worth visiting the site if you want to be walked through the debug process
 * for this file.
 */
#include <iostream>

long factorial(int n);

int main()
{
    int n(0);
    std::cout << "Num: ";
    std::cin >> n;
    long val = factorial(n);
    std::cout << val;
    // std::cin.get();  // You will often see this code online. It's only
                        // needed in full Visual Studio to stop the terminal
                        // from disappearing.
    return 0;
}

long factorial(int n)
{
    long result(1);
    while (n--) {
        result *= n;
    }
    return result;
}
