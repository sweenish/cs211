#include <fstream>
#include <iostream>
#include <string>
#include <vector>

int main()
{
    std::ifstream fin("input.txt");
    if (!fin) {
        std::cerr << "Error opening file...\n";
        return 1;
    }

    int                      numEntries;
    std::vector<std::string> vec;
    fin >> numEntries;
    for (int i = 0; i < numEntries; ++i) {
        std::string tmp;
        std::getline(fin, tmp);
        vec.push_back(tmp);
    }
    fin.close();

    for (auto i : vec) {
        std::cout << i << '\n';
    }
}