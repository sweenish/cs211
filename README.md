# cs211
This repository contains my lecture notes for Wichita State University's CS 211 - Intro to Programming course.

The best way to download the material is through git; this is because I may update materials during the
semester, and git allows you stay in sync with this repository.

## Installing git
* Download and install [git](https://git-scm.com).
    * Windows users: git can optionally be installed through a package manager like chocolatey or
    scoop.
    * Mac users: git can optionally be installed through a package manager like homebrew.
    * Benefits of using a package manager include ease of updating; git does not auto-update.
        * Keeping git up to date is not necessary for this course.

You can ensure git is installed correctly and functioning by cloning this repository.
* Issue the following command in the terminal: `git clone https://gitlab.com/sweenish/cs211`
* To do anything beyond cloning, git will require some minor configurations; they are covered during a
  lab session
