---
title: Welcome
author: Adam Sweeney
extensions: [
    file_loader,
    terminal
]
styles:
    style: fruity
---
# Welcome

```file
path: hello.cpp
lang: cpp
```

```terminal8
fish -il
```
---

# A Bit About Me
* BS in Mechanical Engineering from Washington State University
* MS in Computer Science from Wichita State University
    * Worked as a GEEKS Tutor while a student
    * Worked as a Teaching Professor after graduation
* Software Engineer at NetApp
    * Former member of E-Series Coding Standards Committee
* Other interests include racquetball, movies, games (board and video)
* Guilty pleasure song: Never Really Over by Katy Perry

---

# Agenda
* Quick Syllabus Review
* Notes on Lab
* Study Tips
* Knowledge Probe

---

# Quick Syllabus Review

---

# Notes on Lab
* It is possible to complete all coursework using the lab
* You may still want to be able to code on your own computer

---

# Study Tips (1)
* Go over the slides and code before the lecture
* Try to understand what every line of code does
* Write down any questions to ask during lecture
    * If you did this early enough, you may email your questions to me

---

# Study Tips (2)
* Schedule regular study sessions
    * Per the definition of a credit hour, you should be studying an additional 4-8 hours per week
* Focus on a principle
    * Loops, branching, functions, etc.
* Start with the provided code
    * Attempt to understand what each line does
    * If anything is unclear, write it down
* See if the slides provide an answer
* Ask a TA or myself for clarification, explanations, more code
    * http://www.gregcons.com/KateBlog/HowToAskForCCodingHelp.aspx

---

# Falling Behind
* Don't put off getting help
* Speak with a TA or me
* Start visiting GEEKS regularly, armed with questions
* Attend SI sessions, armed with questions
* If you don't know what questions to bring, be willing to ask them
* Own your education

---

# Knowledge Probe
