from enum import Enum


class Genre(Enum):
    DRAMA = (0,)
    HORROR = (1,)
    COMEDY = (2,)
    ACTION = (3,)
    SCI_FI = (4,)
    ROM_COM = 5


# This class introduces type hinting and name hiding
class Film:
    def __init__(self, title: str, year: int, genre: Genre, rating: float) -> None:
        self.__title = title
        self.__year = year
        self.__genre = genre
        self.__rating = rating
        self.__validate_rating()

    def __validate_rating(self) -> None:
        if self.__rating < 0:
            self.__rating = 0
        elif self.__rating > 10:
            self.__rating = 10

    def __str__(self) -> str:
        return f"{self.__title}, {self.__genre.name.title()}, ({self.__year}). {self.__rating:.1f}/10"

    # @property is an example of a decorator. Decorators are used to 'wrap' a function,
    # and modify its behavior. In this case, we are creating getter functions while
    # still hiding our private data.
    @property
    def title(self) -> str:
        return self.__title

    @property
    def year(self) -> int:
        return self.__year

    @property
    def genre(self) -> Genre:
        return self.__genre

    @property
    def rating(self) -> float:
        return self.__rating


# I can name which parameters I am giving arguments to, this can help with readability
# If I am naming the paremters, order is not important. This is a big difference from
# C++
scott = Film(
    title="Scott Pilgrim vs. The World", rating=7.5, year=2010, genre=Genre.COMEDY
)
print(scott)

# Without the getters, this code will spit out an error because the member __genre is
# "hidden". The leading __ are what indicate this to the python interpreter
print(scott.genre)

# But that data isn't really private
print(scott.__dict__)

# After viewing the object's dictionary, we can now still directly modify the
# class. The concept of private is something enforced at the compiler level,
# and pyhton is not compiled. Simply hiding the names using __ is sufficient
# if you want to steer users toward your available functions.
print(f"Direct access: {scott._Film__year}\nBetter access: {scott.year}")
