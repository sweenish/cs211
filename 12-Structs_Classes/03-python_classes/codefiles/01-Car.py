class Car:
    def __init__(self, make, model, year, odometer, isAutomatic=True):
        self.make = make
        self.model = model
        self.year = year
        self.odometer = odometer
        self.isAutomatic = isAutomatic

    def __str__(self):
        return (
            str(self.year)
            + " "
            + self.make
            + " "
            + self.model
            + " is "
            + ("basic." if self.isAutomatic else "fun.")
        )

    def take_trip(self, miles):
        if miles > 0:
            self.odometer += miles


mustang = Car("Ford", "Mustang", 1967, 10000, False)
sonata = Car("Hyundai", "Sonata", 2004, 26)
# print(mustang)  # For just printing the object

# Demonstrates that getters and settters aren't as necessary
# The idea of private doesn't really exist in python
# We'll see in the next file how we can still fake it
print(f"{mustang} It has {mustang.odometer} miles.")
trip = 500
mustang.take_trip(trip)
print(f"After a {trip} mile trip, my mustang has {mustang.odometer} miles.")
print(sonata)
