#include <iostream>

int main()
{
    char grades[5] = { 'A', 'C', 'A', 'B', 'F' };

    for (int i = 0; i < 5; i++) {
        std::cout << grades[i] << '\n';
    }

    const int bigSize = 789;
    // constexpr int bigSize = 789;  // Generally better once you're writing
                                     // more modern C++. I will continue to use
                                     // const for consistency's sake
    double big[bigSize];

    for (int i = 0; i < bigSize; ++i) {
        big[i] = 100.0;
    }

    return 0;
}
