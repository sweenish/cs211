#include <fstream>
#include <iomanip>
#include <iostream>

void print_line(const std::string_view& name, char fill, std::ostream& sout = std::cout)
{
    sout.fill(fill);
    sout << std::setw(30) << std::left << name << std::setw(15) << std::right << "(555)555-5555"
         << '\n';
}

int main()
{
    char oldFill = std::cout.fill('.');  // The function fill returns the previous fill character
    print_line("Adam Sweeney", '.');
    print_line("A. Sweeney", '*');
    print_line("A. Ham", '#');
    print_line("Alexander Hamilton", '^');
    print_line("Adam S.", oldFill);

    std::ofstream fout("numbers.txt");
    if (!fout) {
        std::cerr << "PROBLEM\n";
        return 1;
    }

    print_line("Adam Sweeney", '.', fout);
    print_line("A. Sweeney", '*', fout);
    print_line("A. Ham", '#', fout);
    print_line("Alexander Hamilton", '^', fout);
    print_line("Adam S.", oldFill, fout);

    fout.close();

    return 0;
}
