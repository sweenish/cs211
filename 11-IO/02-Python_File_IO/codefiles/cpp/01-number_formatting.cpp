#include <iostream>

int main()
{
    double a = 3.1415926534;
    double b = 2017.0;
    double c = 1.0e-10;  // Same as 1x10^(-10)

    std::cout.precision(5);

    // With the exception of the precision, this is how cout would output the numbers normally
    std::cout << "Default output:\n";
    std::cout << a << "\n" << b << "\n" << c << "\n";

    // Fixed means we never see scientific output
    std::cout << "\nFixed Output:\n";
    std::cout.setf(std::ios::fixed, std::ios::floatfield);  // Ignore the second parameter, it is
    std::cout.setf(std::ios::showpoint);  // Show decimal   // required for this program to ensure
    std::cout.setf(std::ios::showpos);    // Show '+'	    // the switch to scientific output works
    std::cout << a << "\n" << b << "\n" << c << "\n";

    // scientific will show all numbers in scientific notation
    std::cout << "\nScientific Output:\n";
    std::cout.setf(std::ios::scientific, std::ios::floatfield);
    // No need to set showpoint, it is still set
    std::cout.unsetf(std::ios::showpos);
    std::cout << a << "\n" << b << "\n" << c << "\n";

    return 0;
}
