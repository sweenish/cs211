// Compile and run thtis program. It createts a binary file for 02-file_modes.cpp
#include <cstdint>
#include <fstream>
#include <iostream>
#include <random>

int main()
{
    std::ofstream fout("binout", std::ios::out | std::ios::binary);
    if (!fout) {
        std::cerr << "Error opening file, exiting...\n";
        return 1;
    }

    std::mt19937                                prng(std::random_device{}());
    std::uniform_int_distribution<int>          randSize(5, 20);
    std::uniform_int_distribution<std::int32_t> randVal(1, 1000);

    std::int32_t              size = randSize(prng);
    std::vector<std::int32_t> vals;

    for (std::int32_t i = 0; i < size; ++i) {
        vals.push_back(randVal(prng));
    }

    // This is one way of writing binary information to a file. We can also use
    // thet methods we are already familiar with, e.g., fout << i;
    //
    // This method is a bit more common for binary mode.
    for (auto i : vals) {
        fout.write(reinterpret_cast<char*>(&i), sizeof(i));
    }

    std::cout << "Written values in binary:\n";
    for (auto i : vals) {
        std::cout << i << ' ';
    }
    std::cout << '\n';
    fout.close();

    return 0;
}
