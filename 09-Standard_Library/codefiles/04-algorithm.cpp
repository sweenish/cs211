#include <algorithm>  // std::copy, std::fill, std::reverse, std::sort, std::generate_n
#include <array>
#include <functional>  // std::greater
#include <iostream>
#include <iterator>  // std::back_inserter
#include <random>
#include <ranges>

// This is a template function, and it is constrained using something called a
// concept. The concept checks that the Container type is a range.
//
// Templates are the C++ way to write generic code. You'll see that this print
// function is called and given both a std::array and a std::vector. It will
// actually accept any container class that is a range, i.e., has a begin() and
// end() function.
//
// Concepts and ranges belong to C++20. Concepts provide a much nicer way to
// constrain a template. Because this function uses a range-based for loop,
// only containers that will play nicely should be allowed.
template <typename Container>
requires std::ranges::range<Container>
void print(Container const& container)
{
    for (const auto& i : container) {
        std::cout << i << ' ';
    }
    std::cout << '\n';
}

int main()
{
    // Creates an array with capacity and size of 10, also initializes every
    // element, in this case to zero
    std::array<int, 10> first = { 0 };
    std::vector<int>    second;
    std::fill(first.begin(), first.end(), 42);  // Every element is now 42

    print(first);

    // Note types of first and second
    // std::back_inserter allows us to add elements to the vector without having
    // to resize it first
    std::copy(first.begin(), first.end(), std::back_inserter(second));
    print(second);

    std::vector<int> third(10);
    std::iota(third.begin(), third.end(), 1);
    print(third);

    std::reverse(third.begin(), third.end());  // the most straightforward of the bunch
    print(third);

    // std::generate_n is great for programatically generating values.
    std::vector<int> fourth;
    std::generate_n(
      std::back_inserter(fourth), 10,
      [prng = std::mt19937(std::random_device{}()),
       dist = std::uniform_int_distribution<int>(1, 1000)]() mutable { return dist(prng); });
    print(fourth);

    std::sort(fourth.begin(), fourth.end());  // Sorts by default from small to large (ascending)
    print(fourth);

    std::sort(fourth.begin(), fourth.end(), std::greater<int>());  // Can provide your own sort
    print(fourth);

    return 0;
}
